/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * "Jawab disini..."
 * Saat kelas-kelas turunan perlu mewarisi beberapa class berbeda atau mengimplementasikan banyak perilaku. Kelas Pengurangan dan Pertambahan mengimplementasikan interface Kalkulator, sehingga keduanya memiliki kemampuan untuk menangani perhitungan tetapi dengan perilaku yang berbeda.
 * Saat yang ingin ditetapkan adalah kontrak perilaku yang harus diikuti oleh kelas-kelas turunan. Interface Kalkulator menetapkan bahwa setiap implementasi harus menyediakan metode hitung yang mengambil dua argumen operan1 dan operan2 serta mengembalikan hasil perhitungan.
 * Saat kelas-kelas turunan perlu menggabungkan perilaku dari beberapa interface yang berbeda. Kelas Pengurangan dan Pertambahan dapat mengimplementasikan interface lain selain Kalkulator jika diperlukan. 
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}