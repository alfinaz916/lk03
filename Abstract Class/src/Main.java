/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas kalkulator, pertambahan, dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa abstract class akan lebih cocok digunakan daripada interface?
 * "Jawab disini..."
 * Saat metode-metode yang digunakan memiliki implementasi umum yang dapat digunakan oleh kelas turunannya tanpa perlu menulis ulang implementasi yang sama, seperti metode setOperan dan hitung.
 * Saat memiliki variabel instance yang dapat digunakan oleh kelas turunannya. Kelas Pengurangan dan Pertambahan menggunakan variabel operan1 dan operan2, yang dapat diakses dan digunakan langsung oleh metode hitung.
 * Saat memiliki constructor, yang memungkinkan inisialisasi variabel dan memberikan perilaku umum yang sama kepada kelas turunannya. Dalam kalkulator, meskipun constructor tidak ditunjukkan, tetapi dapat ditambahkan ke dalam abstract class Kalkulator jika diperlukan. 
 */

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operand1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operand2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        kalkulator.setOperan(operand1, operand2);
        double result = kalkulator.hitung();
        System.out.println("Hasil: " + result);
    }
}